package demo.learn.spring.v3.fortuneImpl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import demo.learn.spring.v3.service.FortuneService;
import java.util.*;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;

@Component("happyfortuneService")
@Scope("prototype")
public class HappyFortuneServiceImpl implements FortuneService {
	private String[] messages = {
		"# Beware of Dogs",
		"# Hopes everything good",
		"# Dont Trust your friends",
		"# Today is your lucky day",
		"# OMG",
		"# No way",
		"# Try harder",
		"# Be a teacher"
	};



	private Random myrandom = new Random();
	private int indx = myrandom.nextInt( messages.length );

	public HappyFortuneServiceImpl () {
		System.out.println("HappyFortuneServiceImpl #"+indx);
	}
	
	@Override
	public String getFortuneService() {
		return messages[indx];
	}

	@PostConstruct
	public void doStartupStuff() {
		System.out.println("PostConstruct @ HappyFortuneServiceImpl #"+indx);
	}
	@PreDestroy
	public void doCleanupStuff() {
		System.out.println("prototype scope never run PreDestroy @ HappyFortuneServiceImpl #"+indx);
	}

}
