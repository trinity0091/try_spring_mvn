package demo.learn.spring.v3.fortuneImpl;

import demo.learn.spring.v3.service.FortuneService;
import java.util.*;

// import jakarta.annotation.PostConstruct;
// import jakarta.annotation.PreDestroy;



public class HeroFortuneServiceImpl implements FortuneService {
	private String[] messages = {
		"## Beware of Dogs",
		"## Hopes everything good",
		"## Dont Trust your friends",
		"## Today is your lucky day",
		"## OMG",
		"## No way",
		"## Try harder",
		"## Be a teacher"
	};



	private Random myrandom = new Random();
	private int indx = myrandom.nextInt( messages.length );

	public HeroFortuneServiceImpl () {
		System.out.println("HeroFortuneServiceImpl #"+indx);
	}
	
	@Override
	public String getFortuneService() {
		return messages[indx];
	}


	/*


	@PostConstruct
	public void doStartupStuff() {
		System.out.println("PostConstruct @ HeroFortuneServiceImpl #"+indx);
	}
	@PreDestroy
	public void doCleanupStuff() {
		System.out.println("prototype scope never run PreDestroy @ HeroFortuneServiceImpl #"+indx);
	}


	*/
}
