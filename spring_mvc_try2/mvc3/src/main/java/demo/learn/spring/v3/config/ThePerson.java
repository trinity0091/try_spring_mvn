package demo.learn.spring.v3.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ThePerson {

    @Value("${person.name}")
    private String name;

    @Value("${person.email}")
    private String email;

    @Value("${person.sex}")
    private String sex;


    @Value("${person.age}")
    private Integer age;


    @Value("${person.address}")
    private String address;



    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }


    public String getSex() {
        return sex;
    }

    public Integer getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }


    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return getName()+" "+getEmail()+" "+getSex()+" "+getAge().toString()+" "+getAddress();
    }





}
