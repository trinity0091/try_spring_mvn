package demo.learn.spring.v1.coachImpl;

import demo.learn.spring.v1.service.Coach;
import demo.learn.spring.v1.service.FortuneService;

public class SwimCoach implements Coach{

	public SwimCoach () {
	}
    
    @Override
    public String getDailyWorkout() {
        return "Swim 1km Daily";
    }


    private FortuneService fortuneService;

	public SwimCoach (FortuneService fs) {
        this.fortuneService=fs;
	}

    @Override
	public String getFortuneService() {
		return "SwimCoach:: Field Injection>> "+this.fortuneService.getFortuneService();
	}

}
