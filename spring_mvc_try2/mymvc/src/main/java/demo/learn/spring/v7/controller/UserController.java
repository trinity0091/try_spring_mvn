package demo.learn.spring.v7.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.learn.spring.v7.model.User;


@RestController
@RequestMapping("/users")
public class UserController {

    @GetMapping("/list")
    public ResponseEntity<List<User>> getUsers() {
        List<User> users = new ArrayList<>();
        users.add(new User("John", "Doe"));
        users.add(new User("Jane", "Doe"));
        return new ResponseEntity<>(users, HttpStatus.OK);
    }
}
