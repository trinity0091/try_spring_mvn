package demo.learn.spring.v4.config;

import org.springframework.web.WebApplicationInitializer;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;

public class MyWebAppInitializer implements WebApplicationInitializerer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        // Create the Spring application context
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.register(MVCDemoConfig.class);

        // Add the Spring MVC DispatcherServlet
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("dispatcher", new DispatcherServlet(context));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");

        // Log the exposed URL(s)
        for (String servletMapping : dispatcher.getServletMappings()) {
            String urlPattern = servletMapping;
            if (urlPattern.equals("/")) {
                urlPattern = "";
            }
            String baseUrl = String.format("http://%s:%d%s", InetAddress.getLocalHost().getHostAddress(), servletContext.getServerPort(), urlPattern);
            System.out.println("Application started at " + baseUrl);
        }
    }
}
