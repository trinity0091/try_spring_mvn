package demo.learn.spring.v4.coachImpl;

import org.springframework.stereotype.Component;

import demo.learn.spring.v4.service.Coach;
import demo.learn.spring.v4.service.FortuneService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@Component
public class CricketCoach implements Coach {

	@Autowired
	@Qualifier("fortuneServiceImpl")
	private FortuneService fortuneService;

	public CricketCoach () {
	}

	@Override
	public String getDailyWorkout() {
		return "CricketCoach:: Spend 1000 minutes practice";
	}

	@Override
	public String getFortuneService() {
		return "CricketCoach:: Field Injection>> "+this.fortuneService.getFortuneService();
	}
}
