package demo.learn.spring.v9;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import demo.learn.spring.v9.config.WebDispatcherServletConfig;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class WebDispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class[] getRootConfigClasses() {
		return new Class[] { WebDispatcherServletConfig.class };
	}

	@Override
	protected Class[] getServletConfigClasses() {
		return null;
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/app9/*" };
	}

}