package demo.learn.spring.v9.config;

//import jakarta.swing.text.InternationalFormatter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "demo.learn.spring.v901")
public class WebDispatcherServletConfig {

}
