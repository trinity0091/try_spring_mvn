package demo.learn.spring.v4.config;

//import jakarta.swing.text.InternationalFormatter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/*
import java.util.List;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
*/


@Configuration
@EnableWebMvc
@ComponentScan(basePackages="demo.learn.spring.v4")
public class WebDispatcherServletConfig implements WebMvcConfigurer {
    /*
    // failed
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converters.add(converter);
    }
    
    */
    // Define a bean for View Resolver
    @Bean
    public ViewResolver viewResolver()  {

        InternalResourceViewResolver vr= new InternalResourceViewResolver();
        vr.setPrefix("/WEB-INF/view/");
        vr.setSuffix(".jsp");
        return vr;
        

    }

    
}
