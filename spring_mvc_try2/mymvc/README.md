# spring_mvn_try1



## MVC VERSION DETAILS

MVC2 -> Implemented Jetty war run 
MVC3 -> Removed MVC Configuration in XML, by extends DispatchingServlet in Java
MVC4 -> Working in JSP
MVC5 -> 2 dispatcher
MVC9 -> Restful without xml


MVC4 : WebDispatcherServletConfig.java

http://localhost:8080/app4/showForm
http://localhost:8080/app4/home



MVC5  : mvc-core-config-v5.xml

http://localhost:8080/app5/person/b -> bad message
http://localhost:8080/app5/person/get
http://localhost:8080/app5/ok -> 404
http://localhost:8080/app5/person/b -> 406 HTTP ERROR 406 Acceptable representations: [].
http://localhost:8080/app5/person/c -> 500



MVC7  : servlet-context-7.xml

http://localhost:8080/app7/users -> 406
http://localhost:8080/app7/users/list  -> 460 No acceptable representation

mvn jetty:run-war -f "/workspaces/try_spring_mvn/spring_mvc_try2/mymvc/pom.xml"





MVC9 : WebDispatcherServletConfig.java
//working fine in jDK11 pom.xml

http://localhost:8080/app9/customers/list -> 406
http://localhost:8080/app9/customers -> 406

http://172.18.0.17:8080/rest9/customers/list 
http://172.18.0.17:8080/rest9/customers 
http://172.18.0.17:8080/rest9/customers/1


http://172.18.0.17:8080/rest9/c/customers/list 
http://172.18.0.17:8080/rest9/c/customers 
http://172.18.0.17:8080/rest9/c/customers/1


mvn jetty:run-war -Djetty.http.port=7171


http://172.18.0.17:7171/rest9/customers/list
http://172.18.0.17:7171/rest9/customers/1

http://172.18.0.17:8080/rest9/customers/list
http://172.18.0.17:8080/rest9/customers/get/101
http://172.18.0.17:8080/rest9/test

# success
https://8080-trinity0091-tryspringmv-lirk4kt54go.ws-us93.gitpod.io/app2/rest/ok

# fail
https://8080-trinity0091-tryspringmv-lirk4kt54go.ws-us93.gitpod.io/app2/home 








```grep -r '.*' src/main/java/ > all_code.txt```


```
grep -r '.*' /workspace/codespace/lang/java-spring/try_spring_mvn/spring_mvc_try2/mvc9/src/main/java/demo/learn/spring/v9/ >  /workspace/codespace/lang/java-spring/try_spring_mvn/spring_mvc_try2/mvc9/all_code.txt
```








mvn clean install -U






