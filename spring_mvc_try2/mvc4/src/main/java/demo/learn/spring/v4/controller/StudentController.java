package demo.learn.spring.v4.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import demo.learn.spring.v4.service.Student;

@Controller
@RequestMapping("/student")
public class StudentController {
    @RequestMapping("showForm")
    public String showForm(Model theModel) {
        // create student obj
        Student theStudent=new Student();
        // add student obj to the model
        theModel.addAttribute("student", theStudent);

        return "student-form";
    }

    @RequestMapping("processForm")
    public String processForm (  @ModelAttribute("student") Student theStudent) {

        //log the input data
        System.out.println("theStudent: "+theStudent.getFirstName() +" "+ theStudent.getLastName());
        
        return "student-confirmation";
    }
    
}
