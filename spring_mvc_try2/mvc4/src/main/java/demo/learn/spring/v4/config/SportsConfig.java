package demo.learn.spring.v4.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import demo.learn.spring.v4.coachImpl.SwimCoach;
import demo.learn.spring.v4.fortuneImpl.HeroFortuneServiceImpl;
import demo.learn.spring.v4.service.Coach;
import demo.learn.spring.v4.service.FortuneService;

// import demo.learn.spring.v4.*;


@Configuration
@ComponentScan("demo.learn.spring.v4")
@PropertySource("classpath:person.properties")
public class SportsConfig {
    

    // Step  2 Inject Bean Dependency
    @Bean
    public FortuneService myHeroFortune() {
        return  new HeroFortuneServiceImpl();
    }



    // Step 1. Define method to expose bean
    @Bean
    public Coach mySwimCoach() {
        SwimCoach myCoach = new SwimCoach( myHeroFortune() );
        return myCoach;
    }

}
