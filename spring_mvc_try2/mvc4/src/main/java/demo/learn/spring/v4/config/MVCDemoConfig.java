package demo.learn.spring.v4.config;

//import jakarta.swing.text.InternationalFormatter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


@Configuration
@EnableWebMvc
@ComponentScan(basePackages="demo.learn.spring.v4")
public class MVCDemoConfig {
    

    // Define a bean for View Resolver
    @Bean
    public ViewResolver viewResolver()  {

        InternalResourceViewResolver vr= new InternalResourceViewResolver();
        vr.setPrefix("/WEB-INF/view/");
        vr.setSuffix(".jsp");
        return vr;
        

    }
}
