package demo.learn.spring.v4.service;

public interface Coach {
    public String getDailyWorkout();
    default String getFortuneService() {
        return "This is a default method in MyInterface";
    }
}
