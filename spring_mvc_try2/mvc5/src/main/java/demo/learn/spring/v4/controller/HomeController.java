package demo.learn.spring.v4.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	// http://localhost:8080/app/home
	@RequestMapping("/home")
	public String showPage() {
		return "main-menu";
	}
}
