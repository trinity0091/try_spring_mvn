package demo.learn.spring.v4;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import demo.learn.spring.v4.config.WebDispatcherServletConfig;

public class WebDispatcherServletInitializer  extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses(){
        return null;
    } 


    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] { WebDispatcherServletConfig.class };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/app/*" };
    }

    @Override
    protected String getServletName() {
        return "dispatcher1";
    }


    

}
