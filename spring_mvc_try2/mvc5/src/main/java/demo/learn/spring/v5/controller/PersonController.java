package demo.learn.spring.v5.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import demo.learn.spring.v5.model.User;

@RestController
@RequestMapping(value = "/rest")
// @RequestMapping(path = "/pets", method = RequestMethod.GET, consumes="application/json")
public class PersonController {
    @RequestMapping("/ok")
	public String healthCheck() {
		return "OK";
	}

	@RequestMapping(path = "/person/b" , method = RequestMethod.GET )
	public User setPerson(@RequestParam(name = "name", required = false, defaultValue = "Unknown") String name) {
		User person = new User("x12","AA","BB","cc@email.cc");
		//person.setFirstName(name);
		return person;
	}

	@RequestMapping(path = "/person/get" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public User getPerson( ) {
		User person = new User("x12","AA","BB","cc@email.cc");
		//person.setFirstName(name);
		return person;
	}

	@RequestMapping(path = "/person/a", method = RequestMethod.GET, produces = "application/json")
	public User getPersonA( ) {
		User person = new User("x12","AA","BB","cc@email.cc");
		//person.setFirstName(name);
		return person;
	}


	@RequestMapping(path = "/person/c", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<User> getPersonC( ) {
		User person = new User("x12","AA","BB","cc@email.cc");
		//person.setFirstName(name);
		return ResponseEntity.ok( person );
	}
}
