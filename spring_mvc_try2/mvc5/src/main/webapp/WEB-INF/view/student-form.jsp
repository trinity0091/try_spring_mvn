<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<head>
    <body>

        <form:form action="processForm" modelAttribute="student">
            First Name: <form:input path="firstName" />
            <br/> <br/>
            Last Name: <form:input path="lastName" />
            <br/> <br/>
            <input type="submit" value="SUBMIT">
        </form:form>

    </body>
</head>