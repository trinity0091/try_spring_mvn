package demo.learn.spring.v2.coachImpl;

import demo.learn.spring.v2.service.Coach;
import demo.learn.spring.v2.service.FortuneService;

public class SwimCoach implements Coach{

	public SwimCoach () {
	}
    
    @Override
    public String getDailyWorkout() {
        return "Swim 1km Daily";
    }


    private FortuneService fortuneService;

	public SwimCoach (FortuneService fs) {
        this.fortuneService=fs;
	}

    @Override
	public String getFortuneService() {
		return "SwimCoach:: Field Injection>> "+this.fortuneService.getFortuneService();
	}

}
