package demo.learn.spring.v2.coachImpl;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import demo.learn.spring.v2.service.Coach;
import demo.learn.spring.v2.service.FortuneService;

@Component
public class FoodballCoach implements Coach {

	private FortuneService fortuneService;

	public FoodballCoach () {
	}

	@Autowired
	public FoodballCoach( @Qualifier("happyfortuneService") FortuneService fortune) {
		System.out.println("FoodballCoach:: Constructor Injection of FortuneService interface --- using Qualifier ");
		this.fortuneService=fortune;
	}

	@Override
	public String getDailyWorkout() {
		return "FoodballCoach:: Spend 30 minutes on foodball practice";
	}

	@Override
	public String getFortuneService() {
		return "FoodballCoach:: Constructor Injection>> "+this.fortuneService.getFortuneService();
	}
}
