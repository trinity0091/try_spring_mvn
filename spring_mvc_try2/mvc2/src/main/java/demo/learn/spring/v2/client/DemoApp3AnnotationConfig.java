package demo.learn.spring.v2.client;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import demo.learn.spring.v2.service.Coach;
import demo.learn.spring.v2.config.SportsConfig;
import demo.learn.spring.v2.config.ThePerson;

public class DemoApp3AnnotationConfig {

	public static void main(String[] args) {

        // read spring config file

        // Step 3 Read Spring Java Configuration Classes
        AnnotationConfigApplicationContext context = 
                    new AnnotationConfigApplicationContext(SportsConfig.class);


                    System.out.println("Inside v2- AnnotationConfigApplicationContext ");


                    /*
                    // Get Bean
                    Coach MyCoach = context.getBean("theSillyCoach", Coach.class);
        
                    // Call Method on the bean
                    System.out.println(MyCoach.getDailyWorkout());
        
                    System.out.println("-------------------");
        
                    // Auto wire Implementation  Constructor Injection Test
                    Coach theSecCoach = context.getBean("foodballCoach", Coach.class );
                    System.out.println(theSecCoach.getDailyWorkout());
                    System.out.println(theSecCoach.getFortuneService());
        
                    System.out.println("-------------------");
        
        
                    // Auto wire Implementation Tennsi  Setter injection
                    Coach thirdCoach = context.getBean("tennisCoach", Coach.class );
                    System.out.println(thirdCoach.getDailyWorkout());
                    System.out.println(thirdCoach.getFortuneService());
        
        
                    System.out.println("-------------------");
        
                    
                    // Auto wire Implementation  Constructor Injection Test
                    Coach fourthCoach = context.getBean("cricketCoach", Coach.class );
                    System.out.println(fourthCoach.getDailyWorkout());
                    System.out.println(fourthCoach.getFortuneService());
        
                    System.out.println("-------------------");


                    */
                            
                    // Step 4  get Bean from spring Container
                    Coach fourthCoach = context.getBean("mySwimCoach", Coach.class );
                    System.out.println(fourthCoach.getDailyWorkout());
                    System.out.println(fourthCoach.getFortuneService());
        
                    System.out.println("-------------------");




                    ThePerson person = context.getBean("thePerson", ThePerson.class );
                    System.out.println(person.toString());
                    System.out.println("-------------------");


        
                    // Close context
                    context.close();
        }

}
