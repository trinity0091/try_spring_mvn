package demo.learn.spring.v2.client;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.learn.spring.v2.service.Coach;

public class DemoApp2Autowired {

	public static void main(String[] args) {

		    // read spring config file
	          ClassPathXmlApplicationContext context = 
				        new ClassPathXmlApplicationContext("v2-conf-class-applicationContext.xml");

            System.out.println("Inside v2-conf-class-applicationContext.xml ");
            // Get Bean
            Coach MyCoach = context.getBean("theSillyCoach", Coach.class);

            // Call Method on the bean
            System.out.println(MyCoach.getDailyWorkout());

            System.out.println("-------------------");

            // Auto wire Implementation  Constructor Injection Test
            Coach theSecCoach = context.getBean("foodballCoach", Coach.class );
            System.out.println(theSecCoach.getDailyWorkout());
            System.out.println(theSecCoach.getFortuneService());

            System.out.println("-------------------");


            // Auto wire Implementation Tennsi  Setter injection
            Coach thirdCoach = context.getBean("tennisCoach", Coach.class );
            System.out.println(thirdCoach.getDailyWorkout());
            System.out.println(thirdCoach.getFortuneService());


            System.out.println("-------------------");

            
            // Auto wire Implementation  Constructor Injection Test
            Coach fourthCoach = context.getBean("cricketCoach", Coach.class );
            System.out.println(fourthCoach.getDailyWorkout());
            System.out.println(fourthCoach.getFortuneService());

            System.out.println("-------------------");


            // Close context
            context.close();
        
        //ct.close();

	}

}
