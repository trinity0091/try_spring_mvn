package demo.learn.spring.v9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import demo.learn.spring.v9.dao.CustomerDAO;
import demo.learn.spring.v9.model.Customer;
import org.springframework.http.MediaType;


@RestController
public class TestRestController {

    @Autowired
    private CustomerDAO customerDAO;

    @GetMapping("/test")
    @ResponseBody
    public List<Customer> getCustomers() {
        return customerDAO.list();
    }


}
