package demo.learn.spring.v9.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import demo.learn.spring.v9.dao.CustomerDAO;
import demo.learn.spring.v9.model.Customer;
import org.springframework.http.MediaType;




@RestController
@RequestMapping(value = "rest9/customers")
public class CustomerRestController {

	@Autowired
	private CustomerDAO customerDAO;

	@GetMapping("/list")
	public List<Customer> getCustomers() {
		return customerDAO.list();
	}

	@GetMapping("/get/{id}")
	public ResponseEntity<Customer> getCustomer(@PathVariable("id") Long id) {
		Customer customer = customerDAO.get(id);
		if (customer == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(customer, HttpStatus.OK);
	}
}
