package demo.learn.springmvc.whatmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WhatSpringMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(WhatSpringMvcApplication.class, args);
	}

}
