package demo.learn.spring.v5.fortuneImpl;

import org.springframework.stereotype.Component;
import demo.learn.spring.v5.service.FortuneService;
import java.util.*;


@Component
public class FortuneServiceImpl implements FortuneService {

	private Random myrandom = new Random();
	private int indx = myrandom.nextInt( 1000 );

	public FortuneServiceImpl () {
		System.out.println("FortuneServiceImpl #"+indx);
	}
	
	@Override
	public String getFortuneService() {
		return "getFortuneServiceImpl : You will become milionare";
	}

}
