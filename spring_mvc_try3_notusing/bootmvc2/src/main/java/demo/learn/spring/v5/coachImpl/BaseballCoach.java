package demo.learn.spring.v5.coachImpl;

import org.springframework.stereotype.Component;
import demo.learn.spring.v5.service.Coach;


@Component
public class BaseballCoach implements Coach {

	public BaseballCoach () {
	}

	@Override
	public String getDailyWorkout() {
		return "Spend 30 minutes on batting practice";
	}

}
