package demo.learn.spring.v5.coachImpl;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import demo.learn.spring.v5.service.Coach;
import demo.learn.spring.v5.service.FortuneService;

@Component
public class TennisCoach implements Coach {

	private FortuneService fortuneService;

	public TennisCoach () {
	}
	
	@Autowired
	@Qualifier("happyfortuneService")
	public void setFortuneService (FortuneService fortune) {
		System.out.println("TennisCoach:: Setter Injection of FortuneService interface ---- using Qualifier");
		this.fortuneService=fortune;
	}
	
	@Override
	public String getDailyWorkout() {
		return "TennisCoach:: Spend 1000 minutes practice";
	}

	@Override
	public String getFortuneService() {
		return "TennisCoach:: Method or Setter Injection>> "+this.fortuneService.getFortuneService();
	}
}
