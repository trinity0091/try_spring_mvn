package demo.learn.spring.v5.coachImpl;

import org.springframework.stereotype.Component;
import demo.learn.spring.v5.service.Coach;

@Component("theSillyCoach")
public class TrackCoach implements Coach {


    @Override
    public String getDailyWorkout(){
        return "Run 5km ground";
    }

    public TrackCoach() {
    }


}

