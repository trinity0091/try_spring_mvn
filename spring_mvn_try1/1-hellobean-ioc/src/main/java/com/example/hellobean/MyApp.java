package com.example.hellobean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
//import com.example.HelloWorld;


public class MyApp {



  public static void main(String[] args) {
    ApplicationContext context = new ClassPathXmlApplicationContext("1-hellobean-applicationContext.xml");
    HelloWorld obj = (HelloWorld) context.getBean("helloBean");
    obj.printHello();
  }



}
