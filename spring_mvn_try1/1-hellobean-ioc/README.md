# spring_mvn_try1



## Getting started












Sure, I'd be happy to help you create a sample Spring Java Maven project in VS Code!
Here are the steps:
    1. Open VS Code and create a new folder for your project.
    2. Open the terminal in VS Code (Ctrl + `) and navigate to the newly created folder.
    3. Run the following command to create a new Maven project:

    
```javascript
mvn archetype:generate -DgroupId=com.example -DartifactId=demo -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false

```

This command creates a new Maven project with the GroupId "com.example" and the ArtifactId "demo". You can replace these values with your own GroupId and ArtifactId as needed.
    4. Once the project is created, navigate into the project directory:

```bash
cd demo
```

    5. Open the pom.xml file in VS Code and add the following dependencies for Spring:


```xml
<dependencies>
  <dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-context</artifactId>
    <version>5.3.15.RELEASE</version>
  </dependency>
  <dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-web</artifactId>
    <version>5.3.15.RELEASE</version>
  </dependency>
</dependencies>
```


    6. Create a new Java class in the src/main/java/com/example directory with the following code:


```java
package com.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
  public static void main(String[] args) {
    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    HelloWorld obj = (HelloWorld) context.getBean("helloBean");
    obj.printHello();
  }
}
```


    7. Create another Java class in the same directory with the following code:


```java
package com.example;

public class HelloWorld {
  private String message;

  public void setMessage(String message) {
    this.message = message;
  }

  public void printHello() {
    System.out.println("Hello " + message);
  }
}
```


    8. Create a new XML file in the src/main/resources directory with the following code:


```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
  <bean id="helloBean" class="com.example.HelloWorld">
    <property name="message" value="Spring Framework" />
  </bean>
</beans>
```


    9. Save all the files and run the following command in the terminal to build the project:
    
```java
mvn clean package
```


    10. Once the build is complete, run the following command to execute the Java class:


```bash
java -cp target/demo-1.0-SNAPSHOT.jar com.example.App
```



Congratulations, you have successfully created a Spring Java Maven project in VS Code!











spring_mvn_try1/demo/
src/main/resources
src/main/resources











mvn help:evaluate -Dexpression=settings.localRepository
/home/dev/.m2/repository/org/springframework/spring-context/5.3.15/spring-context-5.3.15.jar

 mvn dependency:tree

  
java -cp target/demo-1.0-SNAPSHOT.jar com.example.App 
java -cp -Xdebug  target/demo-1.0-SNAPSHOT.jar:/home/dev/.m2/repository/org/springframework/spring-context/5.3.15/spring-context-5.3.15.jar com.example.App 

java -cp  target/demo-1.0-SNAPSHOT.jar:/home/dev/.m2/repository/org/springframework/spring-context/5.3.15/spring-context-5.3.15.jar com.example.App 



mvn clean package && 
java -jar target/demo-1.0-SNAPSHOT.jar
  // Error: Unable to initialize main class com.example.App
  // Caused by: java.lang.NoClassDefFoundError: org/springframework/context/ApplicationContext

spring_mvn_try1/demo/src/main/java/com/example/App.java





ls spring_mvn_try1/demo/target | grep spring-context

/home/dev/.m2/repository/org/springframework/spring-context/
