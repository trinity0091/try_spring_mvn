package com.example.coach;


public class TrackCoach implements Coach {
    
    private FortuneService fortuneService;

    @Override
    public String getDailyWorkout(){
        return "Run 5km ground";
    }

    public TrackCoach() {
    }

    public TrackCoach(FortuneService fortune) {
        this.fortuneService=fortune;
    }

    @Override
    public String getFortune() {
        return "Just Do "+fortuneService.getFortune();
    }

}

