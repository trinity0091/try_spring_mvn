package com.example.coach;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyApp {

	public static void main(String[] args) {

		// create the object
		// Coach theCoach = new BaseballCoach();
		//Coach theCoach = new TrackCoach();
		ApplicationContext ct= new ClassPathXmlApplicationContext("2-ioc-coach-applicationContext.xml");
        Coach theCoach= ct.getBean("myCoach", Coach.class );
		// use the object
		System.out.println(theCoach.getDailyWorkout());		
	}

}
