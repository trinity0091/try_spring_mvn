package com.example.coach;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HappyApp {

	public static void main(String[] args) {

		// create the object
		// Coach theCoach = new BaseballCoach();
		//Coach theCoach = new TrackCoach();
		ApplicationContext ct= new ClassPathXmlApplicationContext("3-di-const-coach-applicationContext.xml");
        Coach theCoach= ct.getBean("myCoach", Coach.class );
		// use the object
		System.out.println(theCoach.getDailyWorkout());	

		
		// let's call our new method for fortunes
		System.out.println(theCoach.getFortune());


        
        //ct.close();

	}

}
