package com.example.coach;

public interface Coach {
    public String getDailyWorkout();
    public String getFortune();
}
