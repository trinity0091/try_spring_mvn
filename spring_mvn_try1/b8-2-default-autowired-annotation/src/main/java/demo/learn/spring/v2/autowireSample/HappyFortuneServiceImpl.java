package demo.learn.spring.v2.autowireSample;

import org.springframework.stereotype.Component;
import demo.learn.spring.v2.service.FortuneService;


//@Component("happyfortuneService")
public class HappyFortuneServiceImpl implements FortuneService {


	public HappyFortuneServiceImpl () {
	}
	
	@Override
	public String getFortuneService() {
		return "getHappyFortuneServiceImpl : Happy be happy";
	}

}
