package demo.learn.spring.v2.componentsSample;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import demo.learn.spring.v2.service.Coach;

public class AnnotationDemoApp {

	public static void main(String[] args) {

		    // read spring config file
	          ClassPathXmlApplicationContext context = 
				        new ClassPathXmlApplicationContext("applicationContext.xml");
            //ApplicationContext ct = new ClassPathXmlApplicationContext("81-annotation-component-applicationContext.xml");

            System.out.println(" Inside 8.1 default-component-annotation ");
            // Get Bean
            Coach MyCoach = context.getBean("theSillyCoach", Coach.class);

            // Call Method on the bean
            System.out.println(MyCoach.getDailyWorkout());




            Coach theSecCoach = context.getBean("baseballCoach", Coach.class );
            System.out.println(theSecCoach.getDailyWorkout());

            // Close context
            context.close();
        
        //ct.close();

	}

}
