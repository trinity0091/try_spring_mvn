package demo.learn.spring.v3.client;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import demo.learn.spring.v3.service.Coach;

public class AnnotationDemoApp {

	public static void main(String[] args) {

		    // read spring config file
	          ClassPathXmlApplicationContext context = 
				        new ClassPathXmlApplicationContext("v3-qualifier-annotation-applicationContext.xml");
            //ApplicationContext ct = new ClassPathXmlApplicationContext("81-annotation-component-applicationContext.xml");

            System.out.println(" Inside IOC Container with v3-qualifier-annotation-applicationContext.xml ");
            // Get Bean
            Coach MyCoach = context.getBean("theSillyCoach", Coach.class);

            // Call Method on the bean
            System.out.println(MyCoach.getDailyWorkout());




            Coach theSecCoach = context.getBean("baseballCoach", Coach.class );
            System.out.println(theSecCoach.getDailyWorkout());

            // Close context
            context.close();
        
        //ct.close();

	}

}
