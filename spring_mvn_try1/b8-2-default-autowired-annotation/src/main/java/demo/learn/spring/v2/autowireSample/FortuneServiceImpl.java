package demo.learn.spring.v2.autowireSample;

import org.springframework.stereotype.Component;
import demo.learn.spring.v2.service.FortuneService;


@Component("fortuneServiceImpl")
public class FortuneServiceImpl implements FortuneService {


	public FortuneServiceImpl () {
	}
	
	@Override
	public String getFortuneService() {
		return "getFortuneServiceImpl : You will become milionare";
	}

}
