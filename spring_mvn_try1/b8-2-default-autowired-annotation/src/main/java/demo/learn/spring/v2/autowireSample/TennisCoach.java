package demo.learn.spring.v2.autowireSample;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import demo.learn.spring.v2.service.Coach;
import demo.learn.spring.v2.service.FortuneService;

@Component
public class TennisCoach implements Coach {

	private FortuneService fortuneService;

	public TennisCoach () {
	}
	
	@Autowired
	//@Qualifier("happyfortuneService")
	public void setFortuneService (FortuneService fortune) {
		System.out.println("Setter Injection of FortuneService interface ");
		this.fortuneService=fortune;
	}
	
	@Override
	public String getDailyWorkout() {
		return "TennisCoach: Spend 1000 minutes practice";
	}

	@Override
	public String getFortuneService() {
		return "Method or Setter Injection>> "+this.fortuneService.getFortuneService();
	}
}
