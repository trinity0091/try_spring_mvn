package demo.learn.spring.v1.autowireSample;

import org.springframework.stereotype.Component;
import demo.learn.spring.v1.service.FortuneService;


@Component //("myfortuneService")
public class FortuneServiceImpl implements FortuneService {


	public FortuneServiceImpl () {
	}
	
	@Override
	public String getFortuneService() {
		return "getFortuneServiceImpl : Happy be happy";
	}

}
