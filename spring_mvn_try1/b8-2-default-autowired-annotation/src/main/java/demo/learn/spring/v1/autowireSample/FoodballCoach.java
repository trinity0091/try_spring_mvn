package demo.learn.spring.v1.autowireSample;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import demo.learn.spring.v1.service.Coach;
import demo.learn.spring.v1.service.FortuneService;

@Component
public class FoodballCoach implements Coach {

	private FortuneService fortuneService;

	public FoodballCoach () {
	}

	@Autowired
	// @Qualifier("myfortuneService")
	public FoodballCoach (FortuneService fortune) {
		this.fortuneService=fortune;
	}

	@Override
	public String getDailyWorkout() {
		return "Spend 30 minutes on batting practice";
	}

	@Override
	public String getFortuneService() {
		return this.fortuneService.getFortuneService();
	}
}
