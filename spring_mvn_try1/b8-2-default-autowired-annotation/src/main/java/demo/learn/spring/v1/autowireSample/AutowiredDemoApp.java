package demo.learn.spring.v1.autowireSample;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.learn.spring.v1.service.Coach;

public class AutowiredDemoApp {

	public static void main(String[] args) {

		    // read spring config file
	          ClassPathXmlApplicationContext context = 
				        new ClassPathXmlApplicationContext("v1-annotation-applicationContext.xml");
            //ApplicationContext ct = new ClassPathXmlApplicationContext("81-annotation-component-applicationContext.xml");

            System.out.println(" Inside 8.1 default-component-annotation ");
            // Get Bean
            Coach MyCoach = context.getBean("theSillyCoach", Coach.class);

            // Call Method on the bean
            System.out.println(MyCoach.getDailyWorkout());



            // Auto wire Implementation Foodball Test
            Coach theSecCoach = context.getBean("foodballCoach", Coach.class );
            System.out.println(theSecCoach.getDailyWorkout());
            System.out.println(theSecCoach.getFortuneService());

            // Close context
            context.close();
        
        //ct.close();

	}

}
