package demo.learn.spring.v2.autowireSample;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import demo.learn.spring.v2.service.Coach;
import demo.learn.spring.v2.service.FortuneService;

@Component
public class FoodballCoach implements Coach {

	private FortuneService fortuneService;

	public FoodballCoach () {
	}

	@Autowired //@Qualifier("fortuneServiceImpl")
	public FoodballCoach (FortuneService fortune) {
		System.out.println("Constructor Injection of FortuneService interface ");
		this.fortuneService=fortune;
	}

	@Override
	public String getDailyWorkout() {
		return "Spend 30 minutes on foodball practice";
	}

	@Override
	public String getFortuneService() {
		return "Constructor Injection>> "+this.fortuneService.getFortuneService();
	}
}
