package com.example.coach;

public class BaseballCoach implements Coach {

	private FortuneService fortuneService;

	public BaseballCoach ( FortuneService fortune) {
		this.fortuneService = fortune;
	}
	
	public BaseballCoach () {
	}
	
	@Override
	public String getDailyWorkout() {
		return "Spend 30 minutes on batting practice";
	}

    @Override
    public String getFortune() {
        return "Just Do "+fortuneService.getFortune();
    }

}
