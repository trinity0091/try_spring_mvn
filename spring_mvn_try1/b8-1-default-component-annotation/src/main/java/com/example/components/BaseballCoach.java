package com.example.components;

import org.springframework.stereotype.Component;

@Component
public class BaseballCoach implements Coach {


	public BaseballCoach () {
	}
	
	@Override
	public String getDailyWorkout() {
		return "Spend 30 minutes on batting practice";
	}

}
