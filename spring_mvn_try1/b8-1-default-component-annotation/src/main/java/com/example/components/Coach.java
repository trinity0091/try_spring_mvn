package com.example.components;

public interface Coach {
    public String getDailyWorkout();
}
