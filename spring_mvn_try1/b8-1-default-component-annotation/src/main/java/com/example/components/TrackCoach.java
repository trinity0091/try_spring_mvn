package com.example.components;

import org.springframework.stereotype.Component;


@Component("theSillyCoach")
public class TrackCoach implements Coach {


    @Override
    public String getDailyWorkout(){
        return "Run 5km ground";
    }

    public TrackCoach() {
    }


}

